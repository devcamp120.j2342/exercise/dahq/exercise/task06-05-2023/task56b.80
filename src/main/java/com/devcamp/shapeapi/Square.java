package com.devcamp.shapeapi;

public class Square extends Rectangle {

    public Square(String color, boolean filled) {
        super(color, filled);

    }

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);

    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setHeight(double side) {
        super.setHeight(side);
    }

    @Override
    public String toString() {
        return "Square+" + super.toString() + " []";
    }

}

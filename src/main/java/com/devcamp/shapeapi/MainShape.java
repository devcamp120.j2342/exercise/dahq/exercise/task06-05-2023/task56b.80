package com.devcamp.shapeapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainShape {

    @CrossOrigin
    @GetMapping("/circle-area")
    public double AreaCircle(@RequestParam(required = true, name = "radius") double radius) {
        Circle circle = new Circle(radius);
        System.out.println(circle.toString());
        return circle.getArea();

    }

    @GetMapping("/circle-perimeter")
    public double perimeterCircle(@RequestParam(required = true, name = "radius") double radius) {
        Circle circle = new Circle("Green", false, radius);
        System.out.println(circle.toString());
        return circle.getPerimeter();

    }

    @GetMapping("/rectangle-area")
    public double areaRectangle(@RequestParam(required = true, name = "width") double width,
            @RequestParam(required = true, name = "height") double height) {
        Rectangle rectangle = new Rectangle("Pink", false, width, height);
        System.out.println(rectangle.toString());
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double perimeterRectangle(@RequestParam(required = true, name = "width") double width,
            @RequestParam(required = true, name = "height") double height) {
        Rectangle rectangle = new Rectangle("Black", false, width, height);
        System.out.println(rectangle.toString());
        return rectangle.getPerimeter();
    }

    @GetMapping("/square-area")
    public double areaSquare() {
        Square square = new Square("Orange", false, 7);
        System.out.println(square.toString());
        return square.getArea();

    }

    @GetMapping("/square-perimeter")
    public double perimeterSquare() {
        Square square = new Square("Orange", false, 7);
        System.out.println(square.toString());
        return square.getPerimeter();

    }

}

package com.devcamp.shapeapi;

public class Rectangle extends Sharp {
    public Rectangle(String color, boolean filled) {
        super(color, filled);

    }

    private double width = 1.0;
    private double height = 1.0;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Rectangle(String color, boolean filled, double width, double height) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle+" + super.toString() + " [width=" + width + ", height=" + height + "]";
    }

    public double getArea() {
        return this.width * this.height;
    }

    public double getPerimeter() {
        return 2 * (this.height + this.width);
    }

}

package com.devcamp.shapeapi;

public class Circle extends Sharp {
    private double radius = 1.0;

    public Circle(String color, boolean filled) {
        super(color, filled);

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle" + super.toString() + " [radius=" + radius + "]";
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getPerimeter() {
        return 2 * (Math.PI * this.radius);
    }

}
